#!/usr/bin/env python
# The goal of this exercise is to convert a string to
# a new string where each character in the new string is '('
# if that character appears only once in the original string,
# or ')' if that character appears more than once in the original string.
# Ignore capitalization when determining if a character is a duplicate.
#
# Examples:
#    "din" => "((("
#    "recede" => "()()()"
#    "Success" => ")())())"
#    "(( @" => "))(("


def duplicate_encode(word):
    a = ""
    word = word.lower()
    for i in range(len(word)):
        if word.find((word[i]), i+1, len(word)) != -1 or \
                        word.find((word[i]), 0, i) != -1:
            a += ")"
        else:
            a += "("

    return a

print(duplicate_encode("din") == "(((")
print(duplicate_encode("recede") == "()()()")
print(duplicate_encode("Success") == ")())())")
print(duplicate_encode("(( @") == "))((")
print(duplicate_encode("A@avVrUuvVE!e#") == ")()))()))))()(")
