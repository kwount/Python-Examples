#!/usr/bin/env python
# Bob is preparing to pass IQ test.
# The most frequent task in this test is to find out which one of the given numbers differs from the others.
# Bob observed that one number usually differs from the others in evenness.
# Help Bob — to check his answers, he needs a program that among the given numbers finds one
# that is different in evenness, and return a position of this number.

#! Keep in mind that your task is to help Bob solve a real IQ test,
# which means indexes of the elements start from 1 (not 0)
#
# Examples:
# iq_test("2 4 7 8 10") => 3 // Third number is odd, while the rest of the numbers are even
# iq_test("1 2 1 1") => 2 // Second number is even, while the rest of the numbers are odd
# iq_test("1 2 2") => 1

# Another solution:
# def iq_test(numbers):
#     e = [int(i) % 2 == 0 for i in numbers.split()]
#     return e.index(True) + 1 if e.count(True) == 1 else e.index(False) + 1


def iq_test(numbers):
    numbers_should_be_even = 0
    num = [int(n) for n in numbers.split()]
    if num[0] % 2 == 0:
        if num[1] % 2 == 0:
            numbers_should_be_even = 1
        elif num[2] % 2 == 0:
            return 2 # previous value plus 1
        else:
            return 1 # first nums[0] value plus 1
    else:
        if num[1] % 2 == 1:
            numbers_should_be_even = 0
        elif num[2] % 2 == 1:
            return 2 # previous value plus 1
        else:
            return 1 # first nums[0] value plus 1
    for i in range(len(num)):
        if num[i] % 2 != 0 and  numbers_should_be_even == 1:
            return i+1
        elif num[i] % 2 == 0 and  numbers_should_be_even == 0:
            return i+1

    return 0

if iq_test("1 2 1 1") == 2 and \
   iq_test("2 4 7 8 10") == 3 and \
   iq_test("1 2 2") == 1 and \
   iq_test("2 2 2") == 0 and \
   iq_test("2 4 12 8 10 7") == 6:
    print("Ok")
else:
    print("FAILURE")